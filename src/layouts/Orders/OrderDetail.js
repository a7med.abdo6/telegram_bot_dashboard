import Grid from "@mui/material/Grid";
import { useState, useEffect } from "react";

import MDBox from "components/MDBox";
import Button from "@mui/material/Button";

import DashboardLayout from "examples/LayoutContainers/DashboardLayout";
import DashboardNavbar from "examples/Navbars/DashboardNavbar";
import Footer from "examples/Footer";
import Card from "@mui/material/Card";

import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Posts from "./posts";
import backgroundImage from "assets/images/bot2.webp";
import TextField from "@mui/material/TextField";
import { useCreatePostApi } from "apis/Posts";

function Index() {
  const [header, setheader] = useState("");
  const [text, settext] = useState("");
  const [chat_id, setchat_id] = useState("");

  const [value, setValue] = useState("Controlled");
  const { mutate: createPostApi } = useCreatePostApi();

  const handlesubmit = (event) => {
    setValue(event.target.value);
    createPostApi({ header, text, chat_id });
  };

  return (
    <DashboardLayout>
      <DashboardNavbar />
      <MDBox mb={2} />

      <MDBox position="relative" mb={5}>
        <MDBox
          display="flex"
          alignItems="center"
          position="relative"
          minHeight="18.75rem"
          borderRadius="xl"
          sx={{
            backgroundImage: ({ functions: { rgba, linearGradient }, palette: { gradients } }) =>
              `${linearGradient(
                rgba(gradients.info.main, 0.6),
                rgba(gradients.info.state, 0.6)
              )}, url(${backgroundImage})`,
            backgroundSize: "cover",
            backgroundPosition: "50%",
            overflow: "hidden",
          }}
        />
        <Card
          sx={{
            position: "relative",
            mt: -8,
            mx: 3,
            py: 2,
            px: 2,
          }}
        >
          <Grid container spacing={1} alignItems="self-end">
            <Grid item xs={12} md={7} lg={7}>
              <Typography style={{ marginBottom: "15px" }}> Order</Typography>

              <Grid container spacing={2}>
                <Grid item xs={10}>
                  <TextField
                    autoFocus
                    disabled
                    margin="dense"
                    id="user_name"
                    label="user name "
                    fullWidth
                    variant="outlined"
                    // value={user_name}
                    // onChange={(e) => setuser_name(e.target.value)}
                  />
                </Grid>
                <Grid item xs={10}>
                  <TextField
                    autoFocus
                    disabled
                    margin="dense"
                    id="order_date"
                    label="order_date "
                    type="text"
                    fullWidth
                    variant="outlined"
                    //value={email}
                    // onChange={(e) => setemail(e.target.value)}
                  />
                </Grid>
                <Grid item xs={10}>
                  <TextField
                    autoFocus
                    margin="dense"
                    disabled
                    id="chat_id"
                    label="chat id "
                    fullWidth
                    variant="outlined"
                    //value={chat_id}
                    //onChange={(e) => setchat_id(e.target.value)}
                  />
                </Grid>
                <Grid item xs={10}>
                  <TextField
                    autoFocus
                    disabled
                    margin="dense"
                    id="contact"
                    label=" contact "
                    fullWidth
                    variant="outlined"
                    //value={contact}
                    // onChange={(e) => setcontact(e.target.value)}
                  />
                </Grid>{" "}
                <Grid item xs={10}>
                  <TextField
                    autoFocus
                    margin="dense"
                    disabled
                    id="bank"
                    label=" bank "
                    fullWidth
                    variant="outlined"
                    // value={bank}
                    //  onChange={(e) => setbank(e.target.value)}
                  />
                </Grid>{" "}
                <Grid item xs={10}>
                  <TextField
                    autoFocus
                    disabled
                    margin="dense"
                    id="bank_no"
                    label="bank no  "
                    fullWidth
                    variant="outlined"
                    // value={bank_no}
                    //onChange={(e) => setbank_no(e.target.value)}
                  />
                </Grid>
                <Grid item xs={10}>
                  <TextField
                    autoFocus
                    margin="dense"
                    id="wallet"
                    disabled
                    label=" wallet "
                    fullWidth
                    variant="outlined"
                    // value={wallet}
                    // onChange={(e) => setwallet(e.target.value)}
                  />
                </Grid>{" "}
                <Grid item xs={10}>
                  <TextField
                    autoFocus
                    margin="dense"
                    disabled
                    id="wallet_add"
                    label=" wallet address "
                    fullWidth
                    variant="outlined"
                    //value={wallet_add}
                    // onChange={(e) => setwallet_add(e.target.value)}
                  />
                </Grid>{" "}
                <Grid item xs={10}>
                  <TextField
                    autoFocus
                    margin="dense"
                    disabled
                    id="kyc_status"
                    label=" kyc_status "
                    fullWidth
                    variant="outlined"
                    // value={kyc_status}
                    // onChange={(e) => setkyc_status(e.target.value)}
                  />
                </Grid>{" "}
                <Grid item xs={10}>
                  <TextField
                    autoFocus
                    margin="dense"
                    disabled
                    id="active"
                    label=" active "
                    fullWidth
                    variant="outlined"
                  />
                </Grid>{" "}
              </Grid>
            </Grid>
            <Grid item xs={12} md={5} lg={5}>
              <Typography>Send Message</Typography>
              <div
                style={{
                  margin: "auto",
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                <TextField
                  id="outlined-multiline-flexible"
                  label="Telegram ID"
                  multiline
                  maxRows={4}
                  style={{ width: "100%", marginBlock: "15px" }}
                  onChange={(e) => setchat_id(e.target.value)}
                  value={chat_id}
                />
                <TextField
                  id="outlined-multiline-flexible"
                  label="Header"
                  multiline
                  maxRows={4}
                  style={{ width: "100%", marginBlock: "15px" }}
                  onChange={(e) => setheader(e.target.value)}
                  value={header}
                />
                <br />
                <TextField
                  id="outlined-multiline-static"
                  label="Message"
                  multiline
                  style={{ width: "100%" }}
                  rows={4}
                  onChange={(e) => settext(e.target.value)}
                  value={text}
                />
                <Button
                  style={{ width: "50%", color: "white", marginBlock: "15px" }}
                  variant="contained"
                  onClick={handlesubmit}
                >
                  Send
                </Button>
              </div>
            </Grid>
          </Grid>
        </Card>
      </MDBox>

      <Footer />
    </DashboardLayout>
  );
}

export default Index;
